package com.company;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        Map<Character, Integer> cnt = new HashMap<>();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(
                                new File("input.txt")
                        )
                )
        );

        String s = null;
        while ((s = in.readLine()) != null) {
            for (Character c : s.toCharArray()) {
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                    int val = 1;
                    if (cnt.containsKey(c)) val += cnt.get(c);
                    cnt.put(c, val);
                }
            }
        }
        System.out.println(cnt);
    }
}
