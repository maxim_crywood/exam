package ru.kpfu.itis.app.acpect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Maxim Yagafarov
 **/
@Aspect
@Configuration
public class SQLInjectionCheckAspect {



    @Around("execution(* ru.kpfu.itis.app.SomeSQLClass.execute(String))")
    public void checkSQL(ProceedingJoinPoint jp) throws Throwable {
        /*
        попытался сделать следующее :
        с помощью регулярок описал только те запросы которые
        могут случиться в моем приложении также написал регулярку для того что точно
        не может быть в нормальном sql запросе в моем приложении
         */
        String query = ((String) jp.getArgs()[0]).toLowerCase();
        Pattern good = Pattern.compile(
                        "select +\\* +[a-z]+ +where +id += +[0-9]+|" +
                        "select +\\* +[a-z]+|" +
                                "insert +into +[a-z]+ +\\( *( *[a-z0-9]+ *, *)* *[a-z0-9]+ *\\)\\s*+values\\( *([a-z0-9]+ *, *)* *[a-z0-9]+ *\\)|" +
                                "update +[a-z]+ +set +( *[a-z0-9]+ *= *[a-z0-9]+ *, *)*( *[a-z0-9]+ *= *[a-z0-9]+ * *) +where +id += +[0-9]+"
        );
        Pattern bad = Pattern.compile(
                "drop|or|;"
        );
        Matcher matcherGood = good.matcher(query);
        Matcher matcherBad = good.matcher(query);
        if (matcherGood.matches() && !matcherBad.matches()) {
            jp.proceed();
        } else {
            throw new RuntimeException("SQL INJECTION query = " + query);
        }
    }
}
