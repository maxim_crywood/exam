package ru.kpfu.itis.app.acpect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Maxim Yagafarov
 **/

@Aspect
@Configuration
public class SetEmailCheckAspect {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.matches();
    }

    @Before("execution(* ru.kpfu.itis.app.*.setEmail(String))")
    public void checkEmail(JoinPoint jp) {
        String s = (String) jp.getArgs()[0];
        if (validate(s)) {
            // ok
        } else {
            throw new RuntimeException("email in setEmail is invalid email = " + s + " , JoinPoint = " + jp);
        }
    }
}

