import org.junit.Test;
import ru.kpfu.itis.app.SomeModel;
import ru.kpfu.itis.app.SomeSQLClass;

/**
 * Maxim Yagafarov
 **/


public class TestAspects {

    @Test
    public void testSetEmail() {
        SomeModel someModel = new SomeModel();
        someModel.setEmail("lolkek");
        assert(someModel.getEmail() == null);
    }

    @Test
    public void testSQLEx() {
        SomeSQLClass ob = new SomeSQLClass();
        ob.execute("drop");
    }
}
